use aoc::run;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();

    let day = args[1].parse::<u8>().unwrap();
    let part = args[2].parse::<u8>().unwrap();

    println!("Day {}, part {}", day, part);
    println!(
        "{}",
        run(
            day,
            part,
            &("input/".to_owned() + &day.to_string() + "_input.txt")
        )
    );
}
