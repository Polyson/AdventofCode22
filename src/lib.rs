use std::cell::RefCell;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;
use std::rc::{Rc, Weak};

pub fn run(day: u8, part: u8, input: &str) -> String {
    let contents = fs::read_to_string(input).expect("Should have been able to read the file");

    match (day, part) {
        (1, 1) => day1a(&contents),
        (1, 2) => day1b(&contents),
        (2, 1) => day2a(&contents),
        (2, 2) => day2b(&contents),
        (3, 1) => day3a(&contents),
        (3, 2) => day3b(&contents),
        (4, 1) => day4a(&contents),
        (4, 2) => day4b(&contents),
        (5, 1) => day5a(&contents),
        (5, 2) => day5b(&contents),
        (6, 1) => day6a(&contents),
        (6, 2) => day6b(&contents),
        (7, 1) => day7a(&contents),
        (7, 2) => day7b(&contents),
        (8, 1) => day8a(&contents),
        (8, 2) => day8b(&contents),
        (9, 1) => day9a(&contents),
        (9, 2) => day9b(&contents),
        (10, 1) => day10a(&contents),
        (10, 2) => day10b(&contents),
        _ => "Non-valid date!".to_string(),
    }
}

fn day1a(contents: &str) -> String {
    let mut c: u32 = 0;
    let mut sum: u32 = 0;

    for line in contents.lines() {
        match line.parse::<u32>() {
            Ok(n) => {
                sum += n;
            }
            Err(_) => {
                if sum > c {
                    c = sum
                }
                sum = 0;
            }
        }
    }

    c.to_string()
}

fn day1b(contents: &str) -> String {
    let mut c: Vec<u32> = vec![0, 0, 0];
    let mut sum: u32 = 0;

    for line in contents.lines() {
        match line.parse::<u32>() {
            Ok(n) => {
                sum += n;
            }
            Err(_) => {
                c.push(sum);
                c.sort_by(|x, y| y.partial_cmp(x).unwrap());
                c.pop();
                sum = 0;
            }
        }
    }
    c.push(sum);
    c.sort_by(|x, y| y.partial_cmp(x).unwrap());
    c.pop();

    c.iter().sum::<u32>().to_string()
}

fn day2a(contents: &str) -> String {
    let mut score: u32 = 0;
    let mut o: u32;
    let mut p: u32;

    for line in contents.lines() {
        let mut it = line.chars();
        o = it.next().unwrap() as u32 - 'A' as u32;
        p = it.nth(1).unwrap() as u32 - 'X' as u32;
        score += p + 1;
        if p == o {
            score += 3;
        } else if p == (o + 1) % 3 {
            score += 6;
        }
    }

    score.to_string()
}

fn day2b(contents: &str) -> String {
    let mut score: u32 = 0;
    let mut o: u32;

    for line in contents.lines() {
        let mut it = line.chars();
        o = it.next().unwrap() as u32 - 'A' as u32;
        score += match it.nth(1).unwrap() {
            'X' => (o + 2) % 3 + 1,
            'Y' => o + 4,
            'Z' => (o + 1) % 3 + 7,
            _ => panic!(),
        };
    }

    score.to_string()
}

fn day3a(contents: &str) -> String {
    let mut sum: u32 = 0;
    for line in contents.lines() {
        let (p1, p2) = line.split_at(line.len() / 2);
        for c in p1.chars() {
            if p2.contains(c) {
                sum += if c >= 'a' {
                    c as u32 - 'a' as u32 + 1
                } else {
                    c as u32 - 'A' as u32 + 27
                };
                break;
            }
        }
    }

    sum.to_string()
}

fn day3b(contents: &str) -> String {
    let mut sum: u32 = 0;

    let mut it = contents.lines();
    loop {
        let l1 = match it.next() {
            Some(l) => l,
            None => break,
        };
        let l2 = it.next().unwrap();
        let l3 = it.next().unwrap();

        for c in l1.chars() {
            if l2.contains(c) && l3.contains(c) {
                sum += if c >= 'a' {
                    c as u32 - 'a' as u32 + 1
                } else {
                    c as u32 - 'A' as u32 + 27
                };
                break;
            }
        }
    }

    sum.to_string()
}

fn day4a(contents: &str) -> String {
    let mut count: u32 = 0;

    for line in contents.lines() {
        let mut it1 = line.split(',');
        let mut it2 = it1.next().unwrap().split('-');
        let a1 = it2.next().unwrap().parse::<u32>().unwrap();
        let a2 = it2.next().unwrap().parse::<u32>().unwrap();
        let mut it2 = it1.next().unwrap().split('-');
        let b1 = it2.next().unwrap().parse::<u32>().unwrap();
        let b2 = it2.next().unwrap().parse::<u32>().unwrap();

        if a1 <= b1 && b2 <= a2 || b1 <= a1 && a2 <= b2 {
            count += 1;
        }
    }

    count.to_string()
}

fn day4b(contents: &str) -> String {
    let mut count: u32 = 0;

    for line in contents.lines() {
        let mut it1 = line.split(',');
        let mut it2 = it1.next().unwrap().split('-');
        let a1 = it2.next().unwrap().parse::<u32>().unwrap();
        let a2 = it2.next().unwrap().parse::<u32>().unwrap();
        let mut it2 = it1.next().unwrap().split('-');
        let b1 = it2.next().unwrap().parse::<u32>().unwrap();
        let b2 = it2.next().unwrap().parse::<u32>().unwrap();

        if a1 <= b1 && b1 <= a2 || b1 <= a1 && a1 <= b2 {
            count += 1;
        }
    }

    count.to_string()
}

fn day5a(contents: &str) -> String {
    let mut parts = contents.split("\n\n");

    let mut stacks: Vec<Vec<char>> = Vec::new();

    // Stacks construction.
    let mut drawing: std::iter::Rev<std::str::Lines<'_>> = parts.next().unwrap().lines().rev();
    for _ in drawing.next().unwrap().split_whitespace() {
        stacks.push(Vec::new());
    }

    loop {
        let line: &str = match drawing.next() {
            Some(l) => l,
            None => break,
        };

        let items: Vec<(usize, &str)> = line.match_indices(char::is_alphabetic).collect();
        for (i, item) in items {
            stacks[(i - 1) / 4].push(item.chars().next().unwrap());
        }
    }

    // Stacks deplacements.
    for line in parts.next().unwrap().lines() {
        let mut numbers = line.to_string();
        numbers.retain(|c: char| c.is_numeric() || c.is_whitespace());
        let op: Vec<&str> = numbers.split_whitespace().collect();
        let m = op[0].parse::<usize>().unwrap();
        let f = op[1].parse::<usize>().unwrap() - 1;
        let t = op[2].parse::<usize>().unwrap() - 1;
        for _ in 0..m {
            let item = stacks[f].pop().unwrap();
            stacks[t].push(item);
        }
    }

    // Top crates:
    let mut result = String::new();
    for i in 0..stacks.len() {
        let stack = &stacks[i];
        if !stack.is_empty() {
            result += &stack[stack.len() - 1].to_string();
        }
    }

    return result;
}

fn day5b(contents: &str) -> String {
    let mut parts = contents.split("\n\n");

    let mut stacks: Vec<Vec<char>> = Vec::new();

    // Stacks construction.
    let mut drawing: std::iter::Rev<std::str::Lines<'_>> = parts.next().unwrap().lines().rev();
    for _ in drawing.next().unwrap().split_whitespace() {
        stacks.push(Vec::new());
    }

    loop {
        let line: &str = match drawing.next() {
            Some(l) => l,
            None => break,
        };

        let items: Vec<(usize, &str)> = line.match_indices(char::is_alphabetic).collect();
        for (i, item) in items {
            stacks[(i - 1) / 4].push(item.chars().next().unwrap());
        }
    }

    // Stacks deplacements.
    for line in parts.next().unwrap().lines() {
        let mut numbers = line.to_string();
        numbers.retain(|c: char| c.is_numeric() || c.is_whitespace());
        let op: Vec<&str> = numbers.split_whitespace().collect();
        let m = op[0].parse::<usize>().unwrap();
        let f = op[1].parse::<usize>().unwrap() - 1;
        let t = op[2].parse::<usize>().unwrap() - 1;

        let mut tmp = Vec::new();
        for _ in 0..m {
            tmp.push(stacks[f].pop().unwrap());
        }
        for _ in 0..m {
            stacks[t].push(tmp.pop().unwrap());
        }
    }

    // Top crates:
    let mut result = String::new();
    for i in 0..stacks.len() {
        let stack = &stacks[i];
        if !stack.is_empty() {
            result += &stack[stack.len() - 1].to_string();
        }
    }

    return result;
}

fn day6a(contents: &str) -> String {
    let mut i = 0;
    let mut pass = 4;
    let mut it = contents.chars();
    let mut marker = std::collections::VecDeque::with_capacity(4);
    while pass != 0 {
        let c = it.next().unwrap();
        let mut new_pass = 4;
        for cm in marker.iter().rev() {
            if pass > new_pass {
                break;
            }
            if c == *cm {
                pass = new_pass;
            }
            new_pass -= 1;
        }
        marker.push_back(c);
        if marker.len() > 4 {
            marker.pop_front();
        }

        pass -= 1;
        i += 1;
    }

    i.to_string()
}

fn day6b(contents: &str) -> String {
    let mut i = 0;
    let mut pass = 14;
    let mut it = contents.chars();
    let mut marker = std::collections::VecDeque::with_capacity(14);
    while pass != 0 {
        let c = it.next().unwrap();
        let mut new_pass = 14;
        for cm in marker.iter().rev() {
            if pass > new_pass {
                break;
            }
            if c == *cm {
                pass = new_pass;
            }
            new_pass -= 1;
        }
        marker.push_back(c);
        if marker.len() > 14 {
            marker.pop_front();
        }

        pass -= 1;
        i += 1;
    }

    i.to_string()
}

// …

struct Day7Node {
    size: u64,
    parent: Weak<RefCell<Day7Node>>,
    children: HashMap<String, Rc<RefCell<Day7Node>>>,
}

fn day7a(contents: &str) -> String {
    let mut sum: u64 = 0;
    let root: Rc<RefCell<Day7Node>> = Rc::new_cyclic(|p| {
        RefCell::new(Day7Node {
            size: 0,
            parent: p.clone(),
            children: HashMap::new(),
        })
    });
    let mut node: Rc<RefCell<Day7Node>> = root.clone();

    for line in contents.lines() {
        if line.starts_with("$ ls") {
            // ls
            continue;
        } else if line.starts_with("$ cd") {
            // cd
            let name = line.trim_start_matches("$ cd ");
            let new_node;
            if name == "/" {
                continue;
            } else if name == ".." {
                new_node = node.borrow().parent.upgrade().unwrap();
            } else {
                new_node = node.borrow().children.get(name).unwrap().clone();
            }
            node = new_node;
        } else {
            let parent = Rc::downgrade(&node);
            let children = HashMap::new();
            let name;
            let size;
            if line.starts_with('d') {
                // dir name
                name = line.trim_start_matches("dir ");
                size = 0;
            } else {
                // file size and name
                let args: Vec<&str> = line.split_whitespace().collect();
                name = args[1];
                size = args[0].parse::<u64>().unwrap();
            }
            node.borrow_mut().children.insert(
                name.to_string(),
                Rc::new(RefCell::new(Day7Node {
                    size,
                    parent,
                    children,
                })),
            );
        }
    }

    node = root.clone();
    'a: loop {
        let mut size = 0;
        for (_, file) in &node.clone().borrow_mut().children {
            let new_node = file.borrow_mut();
            if new_node.size == 0 {
                node = file.clone();
                continue 'a;
            }
            size += new_node.size;
        }
        node.borrow_mut().size = size;
        if size <= 100000 {
            sum += size;
        }

        if node.as_ptr() == root.as_ptr() {
            break;
        }

        let new_node = node.borrow().parent.upgrade().unwrap();
        node = new_node;
    }

    sum.to_string()
}

fn day7b(contents: &str) -> String {
    let root: Rc<RefCell<Day7Node>> = Rc::new_cyclic(|p| {
        RefCell::new(Day7Node {
            size: 0,
            parent: p.clone(),
            children: HashMap::new(),
        })
    });
    let mut node: Rc<RefCell<Day7Node>> = root.clone();
    let missing: u64;
    let mut last_name: String;
    let mut best_size: u64;

    for line in contents.lines() {
        if line.starts_with("$ ls") {
            // ls
            continue;
        } else if line.starts_with("$ cd") {
            // cd
            let name = line.trim_start_matches("$ cd ");
            let new_node;
            if name == "/" {
                continue;
            } else if name == ".." {
                new_node = node.borrow().parent.upgrade().unwrap();
            } else {
                new_node = node.borrow().children.get(name).unwrap().clone();
            }
            node = new_node;
        } else {
            let parent = Rc::downgrade(&node);
            let children = HashMap::new();
            let name;
            let size;
            if line.starts_with('d') {
                // dir name
                name = line.trim_start_matches("dir ");
                size = 0;
            } else {
                // file size and name
                let args: Vec<&str> = line.split_whitespace().collect();
                name = args[1];
                size = args[0].parse::<u64>().unwrap();
            }
            node.borrow_mut().children.insert(
                name.to_string(),
                Rc::new(RefCell::new(Day7Node {
                    size,
                    parent,
                    children,
                })),
            );
        }
    }

    node = root.clone();
    'a: loop {
        let mut size = 0;
        for (_, file) in &node.clone().borrow_mut().children {
            let new_node = file.borrow_mut();
            if new_node.size == 0 {
                node = file.clone();
                continue 'a;
            }
            size += new_node.size;
        }
        node.borrow_mut().size = size;

        if node.as_ptr() == root.as_ptr() {
            break;
        }

        let new_node = node.borrow().parent.upgrade().unwrap();
        node = new_node;
    }

    missing = root.borrow().size - 40000000;
    best_size = root.borrow().size;
    last_name = "/".to_string();
    'a: loop {
        for (name, file) in &node.clone().borrow_mut().children {
            let new_node = file.borrow();
            if !new_node.children.is_empty() && new_node.size >= missing {
                if new_node.size < best_size {
                    best_size = new_node.size;
                }
                last_name = name.to_string();
                node = file.clone();
                continue 'a;
            }
        }

        if node.as_ptr() == root.as_ptr() {
            break;
        }

        let new_node = node.borrow().parent.upgrade().unwrap();
        new_node.borrow_mut().children.remove(&last_name);
        node = new_node;
    }

    best_size.to_string()
}

fn day8a(contents: &str) -> String {
    let grid: Vec<Vec<u8>>;
    let h: usize;
    let w: usize;
    let mut count: u32;

    grid = contents
        .lines()
        .map(|line| {
            line.chars()
                .map(|x| x.to_digit(10).unwrap() as u8)
                .collect()
        })
        .collect();
    h = grid[0].len();
    w = grid.len();

    count = 2 * (h as u32 + w as u32 - 2);
    for i in 1..h - 1 {
        'a: for j in 1..w - 1 {
            // Tree i, j
            for x in 0..i {
                if grid[x][j] >= grid[i][j] {
                    break;
                }
                if x == i - 1 {
                    count += 1;
                    continue 'a;
                }
            }
            for x in (i + 1..h).rev() {
                if grid[x][j] >= grid[i][j] {
                    break;
                }
                if x == i + 1 {
                    count += 1;
                    continue 'a;
                }
            }
            for y in 0..j {
                if grid[i][y] >= grid[i][j] {
                    break;
                }
                if y == j - 1 {
                    count += 1;
                    continue 'a;
                }
            }
            for y in (j + 1..w).rev() {
                if grid[i][y] >= grid[i][j] {
                    break;
                }
                if y == j + 1 {
                    count += 1;
                    continue 'a;
                }
            }
        }
    }

    return count.to_string();
}

fn day8b(contents: &str) -> String {
    let grid: Vec<Vec<u8>>;
    let h: usize;
    let w: usize;
    let mut best: u32 = 0;

    grid = contents
        .lines()
        .map(|line| {
            line.chars()
                .map(|x| x.to_digit(10).unwrap() as u8)
                .collect()
        })
        .collect();
    h = grid[0].len();
    w = grid.len();

    for i in 1..h - 1 {
        for j in 1..w - 1 {
            let mut score = 1;
            let mut t = 0;
            for x in (0..i).rev() {
                t += 1;
                if grid[x][j] >= grid[i][j] {
                    break;
                }
            }
            score *= t;
            t = 0;
            for x in i + 1..h {
                t += 1;
                if grid[x][j] >= grid[i][j] {
                    break;
                }
            }
            score *= t;
            t = 0;
            for y in (0..j).rev() {
                t += 1;
                if grid[i][y] >= grid[i][j] {
                    break;
                }
            }
            score *= t;
            t = 0;
            for y in j + 1..w {
                t += 1;
                if grid[i][y] >= grid[i][j] {
                    break;
                }
            }
            score *= t;

            if score > best {
                best = score;
            }
            // println!("Score for ({}, {}) is {}", i, j, score);
        }
    }

    return best.to_string();
}

fn day9a(contents: &str) -> String {
    let mut visited: HashSet<(i32, i32)> = HashSet::new();
    let (mut hx, mut hy): (i32, i32) = (0, 0);
    let (mut tx, mut ty): (i32, i32) = (0, 0);

    visited.insert((tx, ty));
    for (dir, mov) in contents.lines().map(|line| {
        line.split_once(' ')
            .map(|(c, n)| (c.chars().next().unwrap(), n.parse::<i32>().unwrap()))
            .unwrap()
    }) {
        let (mx, my) = match dir {
            'R' => (0, mov),
            'L' => (0, -mov),
            'U' => (mov, 0),
            'D' => (-mov, 0),
            _ => panic!(),
        };
        hx += mx;
        hy += my;
        loop {
            let dx = hx - tx;
            let dy = hy - ty;
            if dx.abs() <= 1 && dy.abs() <= 1 {
                break;
            }
            tx += dx.signum();
            ty += dy.signum();
            visited.insert((tx, ty));
        }
    }

    visited.len().to_string()
}

fn day9b(contents: &str) -> String {
    let mut visited: HashSet<(i32, i32)> = HashSet::new();
    let mut knots: Vec<(i32, i32)> = vec![(0, 0); 10];
    let mut need_check: bool;

    visited.insert(knots.last().unwrap().clone());
    for (dir, mov) in contents.lines().map(|line| {
        line.split_once(' ')
            .map(|(c, n)| (c.chars().next().unwrap(), n.parse::<i32>().unwrap()))
            .unwrap()
    }) {
        let (mx, my) = match dir {
            'R' => (0, mov),
            'L' => (0, -mov),
            'U' => (mov, 0),
            'D' => (-mov, 0),
            _ => panic!(),
        };
        knots[0].0 += mx;
        knots[0].1 += my;
        need_check = true;
        while need_check {
            for i in 1..knots.len() {
                let dx = knots[i - 1].0 - knots[i].0;
                let dy = knots[i - 1].1 - knots[i].1;
                if dx.abs() <= 1 && dy.abs() <= 1 {
                    if i == 1 {
                        need_check = false;
                    }
                    continue;
                }
                knots[i].0 += dx.signum();
                knots[i].1 += dy.signum();
                if i == knots.len() - 2 {
                    visited.insert(knots[knots.len() - 1]);
                }
            }
        }
        visited.insert(knots.last().unwrap().clone());
    }

    visited.len().to_string()
}

fn day10a(contents: &str) -> String {
    let mut sum: i32 = 0;
    let mut cycle: u32 = 1;
    let mut reg: i32 = 1;

    for line in contents.lines() {
        if line.starts_with("addx ") {
            cycle += 2;
            if cycle % 40 == 21 {
                sum += (cycle - 1) as i32 * reg;
            }
            reg += line.trim_start_matches("addx ").parse::<i32>().unwrap();
        } else {
            cycle += 1;
        }

        if cycle % 40 == 20 {
            sum += cycle as i32 * reg;
        }
    }

    sum.to_string()
}

fn day10b(contents: &str) -> String {
    let mut cycle: usize = 0; // - 1
    let mut row: usize = 0;
    let mut reg: i32 = 1;
    let mut crt: [[char; 40]; 6] = [['.'; 40]; 6];

    for line in contents.lines() {
        if reg >= cycle as i32 - 1 && reg <= cycle as i32 + 1 {
            crt[row][cycle] = '#';
        } else {
            crt[row][cycle] = '.';
        }

        if line.starts_with("addx ") {
            cycle = (cycle + 1) % 40;
            if cycle == 0 {
                row = (row + 1) % 6;
            }
            if reg >= cycle as i32 - 1 && reg <= cycle as i32 + 1 {
                crt[row][cycle] = '#';
            } else {
                crt[row][cycle] = '.';
            }
            cycle = (cycle + 1) % 40;
            reg += line.trim_start_matches("addx ").parse::<i32>().unwrap();
        } else {
            cycle = (cycle + 1) % 40;
        }

        if cycle == 0 {
            row = (row + 1) % 6;
        }
    }

    crt.into_iter()
        .map(|r| r.into_iter().collect::<String>() + "\n")
        .collect::<String>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day1_test() {
        assert_eq!("24000", run(1, 1, "input/1_test.txt"));
        assert_eq!("45000", run(1, 2, "input/1_test.txt"));
    }

    #[test]
    fn day2_test() {
        assert_eq!("15", run(2, 1, "input/2_test.txt"));
        assert_eq!("12", run(2, 2, "input/2_test.txt"));
    }

    #[test]
    fn day3_test() {
        assert_eq!("157", run(3, 1, "input/3_test.txt"));
        assert_eq!("70", run(3, 2, "input/3_test.txt"));
    }

    #[test]
    fn day4_test() {
        assert_eq!("2", run(4, 1, "input/4_test.txt"));
        assert_eq!("4", run(4, 2, "input/4_test.txt"));
    }

    #[test]
    fn day5_test() {
        assert_eq!("CMZ", run(5, 1, "input/5_test.txt"));
        assert_eq!("MCD", run(5, 2, "input/5_test.txt"));
    }

    #[test]
    fn day6_test() {
        assert_eq!("7", run(6, 1, "input/6_test.txt"));
        assert_eq!("19", run(6, 2, "input/6_test.txt"));
    }

    #[test]
    fn day7_test() {
        assert_eq!("95437", run(7, 1, "input/7_test.txt"));
        assert_eq!("24933642", run(7, 2, "input/7_test.txt"));
    }

    #[test]
    fn day8_test() {
        assert_eq!("21", run(8, 1, "input/8_test.txt"));
        assert_eq!("8", run(8, 2, "input/8_test.txt"));
    }

    #[test]
    fn day9_test() {
        assert_eq!("13", run(9, 1, "input/9_test1.txt"));
        assert_eq!("1", run(9, 2, "input/9_test1.txt"));
        assert_eq!("36", run(9, 2, "input/9_test2.txt"));
    }

    #[test]
    fn day10_test() {
        assert_eq!("13140", run(10, 1, "input/10_test.txt"));
        assert_eq!(
            "\
##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....
",
            run(10, 2, "input/10_test.txt")
        );
    }
}
